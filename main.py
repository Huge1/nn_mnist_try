""" Starting experiments from https://www.askpython.com/python/examples/load-and-plot-mnist-dataset-in-python """
from keras.datasets import mnist

def set_and_load_data():
    #todo use (two instances of )named tupple instead:
    (train_pic, train_num_on_pic), (test_pic, test_num_on_pic) = mnist.load_data()
    # print(f"type of pics: {train_pic}") # weird lists of zeros
    def show_dataset(pics, selector = None):
        import matplotlib
        matplotlib.use('TkAgg')
        from matplotlib import pyplot
        if not selector:
            for i in range(9):
                pyplot.subplot(330 + 1 + i)
                pyplot.imshow(pics[i], cmap=pyplot.get_cmap('gray'))
            pyplot.show()
        else:
            raise Exception("selector not implemented")
    show_dataset(train_pic[6:])  # awesome, we can scroll/page down like that


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    set_and_load_data()
